#include "Image.hpp"
#include <algorithm>

using namespace cv;


inline float slope(Vec4i line) {

	return float(line[3] - line[1]) / float(line[2] - line[0]);

}

inline float inv_slope(Vec4i line) {
    
    return float(line[2] - line[0]) / float(line[3] - line[1]);
    
}






double median (cv::Mat image)
{
    double m=(image.rows*image.cols)/2;
    int bin0=0;
    cv::Scalar med;
    med.val[0]=-1;
    med.val[1]=-1;
    med.val[2]=-1;
    int histSize = 256;
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    cv::Mat hist0;
    std::vector<cv::Mat> channels;
    cv::split( image, channels );
    cv::calcHist( &channels[0], 1, 0, cv::Mat(), hist0, 1, &histSize, &histRange, uniform, accumulate );
    
    for (int i=0; i<256 && ( med.val[0]<0 || med.val[1]<0 || med.val[2]<0);i++)
    {
        bin0=bin0+cvRound(hist0.at<float>(i));

        if (bin0>m && med.val[0]<0)
            med.val[0]=i;
    }
    
    return med.val[0];
}


cv::Mat toGrayscale(const cv::Mat& mat,ImageType type) {
    
    
    Mat gray;
    
    if (type == RGB) {
        
        cvtColor(mat, gray, COLOR_BGR2GRAY);
        
    }
    
    else if (type == HSV) {
        
        Mat temp;
        cvtColor(mat, temp, COLOR_HSV2BGR);
        cvtColor(temp, gray, COLOR_BGR2GRAY);
        
    }
    
    
    else gray = mat;
    
    return gray;
    
    
}


cv::Mat customEdgeDetectorImage(const cv::Mat &mat,int tau,float cleanPortion,ImageType type) {
	Mat grayscale;
	if (mat.type() != 0)
		grayscale = toGrayscale(mat, type);
	else
		grayscale = mat.clone();
    
    int aux = 0;
    
    cv::Mat output = cv::Mat::zeros(grayscale.size(),grayscale.type());
    
    auto cols = grayscale.cols;
    auto rows = grayscale.rows;
    
    for (int j = (int) (rows * cleanPortion); j < rows; ++j) {
        
        uchar *ptrRowSrc = grayscale.ptr<uchar>(j);
        uchar *ptrRowdst = output.ptr<uchar>(j);
        
        //between srcGray[j][tau] and srcGray[j][cols-tau]
        for (int i = 0; i < cols; ++i)
        {
            
            
            if (i < tau || i > cols - tau) {
                ptrRowdst[i] = 0;
                continue;
            }
            
            if (ptrRowSrc[i] != 0)
            {
                //double that pixel to be analized
                aux = 2 * ptrRowSrc[i];
                //subtract to aux the value of the pixel that is to the left tau units
                aux -= ptrRowSrc[i - tau];
                // subtract to aux the value of the pixel that is to the right tau units
                aux -= ptrRowSrc[i + tau];
                //subtract to aux the value of the diference between left-tau-pixel value and right-tau-pixel value
                aux -= abs((int)ptrRowSrc[i - tau] - ptrRowSrc[i + tau]);
                
                aux = (aux < 0) ? 0 : aux;
                aux = (aux > 255) ? 255 : aux;
                
                ptrRowdst[i] = (uchar)aux;
                
            }
        }
    }
    
    return output;
}



cv::Mat auto_canny(cv::Mat const & mat,float sigma){
    
    double v = median(mat);
    
    int lower = int( max(0.0 , (1.0 - sigma) *v));
    int higher = int(min(255.0 , (1.0 - sigma) *v));
    
    cv::Mat out;
    
    Canny(mat, out, lower, higher);
    
    return out;
    
    
}


void thinning(cv::Mat & input){

    
    for (int row = 0; row < input.rows; row++) {
        
        int whitePixels = 0;
        
        for (int col = 0; col < input.cols; col++){ // for each pixel
            
            uchar val = input.at<uchar>(row,col);
            
            
            
            if (val == 255) { // if its white increment counter
                whitePixels++;
                continue;
            }
            else if(whitePixels > 0){// if the seuqence of white pixels has finished
                
                
                for (int d = 0; d < whitePixels + 1; d++) {// clear all
                    
                    
                    input.at<uchar>(row,col-d) = 0;
                }
                
                input.at<uchar>(row,col-whitePixels/2) = 255; // color the middle one
                
                                
                
            }
            whitePixels = 0;// clear counter
            
            
            
        }
        
        
        
        
    }


}



Image::Image(int rows, int cols, int value) {


	this->matrix = cv::Mat(rows, cols, CV_8UC1, value);
	this->type = RGB;


}

Image::Image(std::string path) {

	this->matrix = cv::imread(path, CV_LOAD_IMAGE_COLOR);
	this->type = RGB;
}

Image::Image(cv::Mat matrix, ImageType type) {


	this->matrix = matrix.clone();
	this->type = type;

}

Image::Image() {


}

void Image::save(std::string path) {



	imwrite(path, this->matrix);

}

cv::Mat Image::getImage() {


	return matrix;


}

Image Image::grayScaleImage() {


	Mat gray;

	if (type == RGB) {

		cvtColor(this->matrix, gray, COLOR_BGR2GRAY);

	}

	else if (type == HSV) {

		Mat temp;
		cvtColor(this->matrix, temp, COLOR_HSV2BGR);
		cvtColor(temp, gray, COLOR_BGR2GRAY);

	}


	else gray = this->matrix.clone();

	return Image(gray, GrayScale);


}

Image Image::saltAndPepperNoiseImage(int noisePercentage) {

	Mat noise = Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8U);

	randu(noise, 0, 100);

	Mat blackMask = noise <= noisePercentage / 2;
	Mat whiteMask = noise >= 100 - noisePercentage / 2;

	Image imageWithNoise = Image(this->matrix.clone(), this->type);

	imageWithNoise.matrix.setTo(255, whiteMask);
	imageWithNoise.matrix.setTo(0, blackMask);

	return imageWithNoise;




}

Image Image::channelImage(ImageChannel channel) {



	auto channels = this->channels();




	if (channel == HUE || channel == SATURATION || channel == VALUE) {


		return Image(channels[channelIndex(channel)], GrayScale);


	}



	switch (channel) {
	case HUE:
	case BLUE:
		channels[channelIndex(RED)] = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC1);
		channels[channelIndex(GREEN)] = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC1);
		break;
	case SATURATION:
	case GREEN:
		channels[channelIndex(RED)] = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC1);
		channels[channelIndex(BLUE)] = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC1);

		break;
	case VALUE:
	case RED:

		channels[channelIndex(GREEN)] = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC1);
		channels[channelIndex(BLUE)] = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC1);

		break;
	default:
		break;
	}




	std::vector<Mat> rgbChannels(3);
	rgbChannels[0] = channels[channelIndex(BLUE)];
	rgbChannels[1] = channels[channelIndex(GREEN)];
	rgbChannels[2] = channels[channelIndex(RED)];


	Mat newM;

	merge(rgbChannels, newM);

	return Image(newM, RGB);









}

Image Image::imageByOffsettingChannel(ImageChannel channel, int offset) {


	//TODO: Fix this to convert to correct type


	auto channels = this->channels();

	channels[channelIndex(channel)] += offset;

	Mat newMat;

	std::vector<Mat> rgbChannels(3);

	rgbChannels[0] = channels[channelIndex(BLUE)];
	rgbChannels[1] = channels[channelIndex(GREEN)];
	rgbChannels[2] = channels[channelIndex(RED)];

	merge(rgbChannels, newMat);

	return Image(newMat, this->type);



}

Image Image::hsvImage() {


	cv::Mat newM;


	if (type == RGB) {
		cvtColor(this->matrix, newM, CV_BGR2HSV);
	}
	else {
		return Image(this->matrix, this->type);
	}



	Image i(newM, HSV);
	i.type = HSV;

	return i;


}

Image Image::rgbImage() {


	cv::Mat newM;

	if (type == HSV) {
		cvtColor(this->matrix, newM, CV_HSV2BGR);
	}
	else if (type == GrayScale) {
		cvtColor(this->matrix, newM, CV_GRAY2BGR);

	}
	else {
		return Image(this->matrix, RGB);
	}


	return Image(newM, RGB);

}

Image Image::histogramImage() {




	auto channels = this->channels();


	/// Establish the number of bins
	int histSize = 256;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	bool uniform = true; bool accumulate = false;

	Mat b_hist, g_hist, r_hist;

	/// Compute the histograms:
	calcHist(&channels[channelIndex(BLUE)], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&channels[channelIndex(GREEN)], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&channels[channelIndex(RED)], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);

	// Draw the histograms for B, G and R
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

	/// Normalize the result to [ 0, histImage.rows ]
	normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

	/// Draw for each channel
	for (int i = 1; i < histSize; i++)
	{
		cv::line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(b_hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
		cv::line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(g_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(g_hist.at<float>(i))),
			Scalar(0, 255, 0), 2, 8, 0);
		cv::line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(r_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(r_hist.at<float>(i))),
			Scalar(0, 0, 255), 2, 8, 0);
	}


	return Image(histImage, RGB);

}

Image Image::equalizedImage() {


	return equalizeUsingTransformer([](cv::Mat input)->cv::Mat {

		Mat output;
		equalizeHist(input, output);

		return output;

	});


}

Image Image::meanFilter(cv::Size size) {

	Mat newMat;
	blur(matrix, newMat, size);
	return Image(newMat, this->type);


}

Image Image::medianFilter(int size) {

	Mat newMat;
	medianBlur(matrix, newMat, size);

	return Image(newMat, this->type);

}

Image Image::gaussianFilter(Size size) {

	Mat newMat;

	GaussianBlur(matrix, newMat, size, 0);

	return Image(newMat, this->type);

}

Image Image::equalizeUsingTransformer(std::function<cv::Mat(cv::Mat frame)>  transformer) {


	Mat dst;

	Image imageToOperateOn;

	if (type == RGB) {

		imageToOperateOn = this->hsvImage();
	}

	else {

		imageToOperateOn = *this;

	}

	Mat valueChannel;


	if (imageToOperateOn.type == HSV) {

		valueChannel = imageToOperateOn.channels()[channelIndex(VALUE)];

	}
	else { // grayscale grayscale

		valueChannel = imageToOperateOn.channels()[0];
	}


	Mat equalizedChannel;

	//equalizeHist(valueChannel, equalizedChannel);

	equalizedChannel = transformer(valueChannel);


	Image equalizedImage;



	if (imageToOperateOn.type == HSV) {


		std::vector<Mat> channelsToUse(3);

		channelsToUse[0] = imageToOperateOn.channels()[channelIndex(HUE)];
		channelsToUse[1] = imageToOperateOn.channels()[channelIndex(SATURATION)];
		channelsToUse[2] = equalizedChannel;

		Mat resultingMatrix;



		merge(channelsToUse, resultingMatrix);

		return Image(resultingMatrix, HSV).rgbImage();

	}

	else {

		return Image(equalizedChannel, GrayScale);

	}





}



Image Image::customEdgeDetectorImage(int tau, bool clean) {

	auto inputImage = this->grayScaleImage();
	int aux = 0;


	cv::Mat output = cv::Mat::zeros(inputImage.matrix.size(), inputImage.matrix.type());

	for (int j = inputImage.matrix.rows/3; j < inputImage.matrix.rows; ++j) {
        
		uchar *ptrRowSrc = inputImage.matrix.ptr<uchar>(j);
		uchar *ptrRowdst = output.ptr<uchar>(j);

		//between srcGray[j][tau] and srcGray[j][cols-tau]
		for (int i = 0; i < inputImage.matrix.cols; ++i)
		{


			if (i < tau || i > inputImage.matrix.cols - tau) {
				ptrRowdst[i] = 0;
				continue;
			}

			if (ptrRowSrc[i] != 0)
			{
				//double that pixel to be analized
				aux = 2 * ptrRowSrc[i];
				//subtract to aux the value of the pixel that is to the left tau units
				aux += -ptrRowSrc[i - tau];
				// subtract to aux the value of the pixel that is to the right tau units
				aux += -ptrRowSrc[i + tau];
				//subtract to aux the value of the diference between left-tau-pixel value and right-tau-pixel value
				aux += -abs((int)ptrRowSrc[i - tau] - ptrRowSrc[i + tau]);

				aux = (aux < 0) ? 0 : aux;
				aux = (aux > 255) ? 255 : aux;

				ptrRowdst[i] = (uchar)aux;

			}
		}
	}

	return Image(output, GrayScale);
}

Image Image::laplacianImage() {


	Mat newMat;
	Laplacian(this->matrix, newMat, CV_8U);

	return Image(newMat, this->type);


}

Image Image::claheImage(double clipLimit, Size tileGridSize) {


	return equalizeUsingTransformer([clipLimit, tileGridSize](cv::Mat input)->cv::Mat {


		auto clahe = createCLAHE(clipLimit, tileGridSize);
		Mat output;
		clahe->apply(input, output);
		return output;

	});


}

Image Image::cannyImage(double t1, double t2, bool overlap) {


	/// Reduce noise with a kernel 3x3

	cv::Mat cannyOutput;

	blur(this->grayScaleImage().matrix, cannyOutput, Size(3, 3));

	/// Canny detector
	Canny(cannyOutput, cannyOutput, t1, t2);

	if (!overlap) {

		return Image(cannyOutput, GrayScale);

	}


	cv::Mat final;

	this->matrix.copyTo(final, cannyOutput);

	return Image(final, this->type);
}


Image Image::floodFillImage(int lowerThreshold, int upperThreshold) {


	Image cannyImage = this->cannyImageAuto(lowerThreshold, upperThreshold);

	cv::copyMakeBorder(cannyImage.matrix, cannyImage.matrix, 1, 1, 1, 1, cv::BORDER_REPLICATE);



	cv::Mat filledMatrix = this->matrix.clone();

	if (this->type == GrayScale) {
		cv::floodFill(filledMatrix, Point(this->matrix.cols / 2, this->matrix.rows - this->matrix.rows / 3), 255, nullptr, 50, 50, 4);
	}
	else {
		cv::floodFill(filledMatrix, Point(this->matrix.cols / 2, this->matrix.rows - this->matrix.rows / 3), CV_RGB(255, 0, 0), nullptr, CV_RGB(20, 20, 20), CV_RGB(20, 20, 20), 4);
	}

	return Image(this->matrix, this->type);
}


Image Image::cannyImageAuto(int &lowerThreshold, int &upperThreshold, int minMeanAllowed, int maxMeanAllowed) {

	cv::Mat cannyInput = this->matrix;
	cv::Mat cannyOutput;

	if (lowerThreshold == MIN_THRESHOLD && upperThreshold == MAX_THRESHOLD) {
		return cannyAutoThreshold(lowerThreshold, upperThreshold, minMeanAllowed, maxMeanAllowed);
	}

	Canny(cannyInput, cannyOutput, lowerThreshold, upperThreshold);

	return Image(cannyOutput, GrayScale);

}


Image Image::cannyAutoThreshold(int &lowerThreshold, int &upperThreshold, int min_mean_allowed, int max_mean_allowed) {



	cv::Mat cannyInput = this->matrix;

	int threashold = (upperThreshold - lowerThreshold) / 2;


	cv::Mat cannyOutput;
	bool stop = false;
	while (!stop) {

		Canny(cannyInput, cannyOutput, threashold, 3 * threashold);
		cv::Scalar avgPixelIntensity = cv::mean(cannyOutput);

		double mean = cv::mean(cannyOutput)[0];

		if (mean > max_mean_allowed) { // increase treashold

			lowerThreshold = threashold;




		}
		else if (mean < min_mean_allowed) {

			upperThreshold = threashold;



		}
		else {
			stop = true;
		}

		auto new_threashold = lowerThreshold + (upperThreshold - lowerThreshold) / 2;



		if (new_threashold < 0 || new_threashold == threashold) {

			stop = true;
		}


		threashold = new_threashold;
	}

	return Image(cannyOutput, GrayScale);
}


std::vector<Mat> Image::channels() {


	std::vector<Mat> vector(this->matrix.channels());

	Mat *channels = new cv::Mat[this->matrix.channels()];
	split(this->matrix, channels);

	for (int i = 0; i < this->matrix.channels(); i++) {
		vector.at(i) = channels[i].clone();

	}

	delete[] channels;

	return vector;

}

std::vector<Image> Image::channelImages() {



	auto channels = this->channels();


	std::vector<Image> images(channels.size());




	std::transform(channels.begin(), channels.end(), images.begin(), [](Mat matrix)->Image {

		return Image(matrix.clone(), GrayScale);

	});

	return images;
}


int Image::channelIndex(ImageChannel channel) {


	switch (channel) {
	case VALUE:
	case RED:
		return 2;
	case SATURATION:
	case GREEN:
		return 1;
	case HUE:
	case BLUE:
		return 0;

	default:
		return 0;
	}



}

void Image::display(std::string name) {

	if (this->matrix.cols == 0 || this->matrix.rows == 0) {
		return;
	}

	namedWindow(name, WINDOW_AUTOSIZE);
	imshow(name, this->matrix);

}

void Image::resetImage() {
	this->matrix.setTo(0);
}



Vec4i extendLine(int minY,int maxY , Vec4i line){
    
    
    auto m = slope(line);
	if (m != m) {
		return Vec4i{ int(line[2]),minY,int(line[2]),maxY };
	}
    return Vec4i{int((minY - line[3])/m + line[2]),minY,int((maxY - line[3])/m + line[2]),maxY};
    
}



vector<Point2f> extendLine(int minY,int maxY , Point2f pt1,Point2f pt2){
    
    
    Vec4i line = {int(pt1.x),int(pt1.y),int(pt2.x),int(pt2.y)};
    
    Vec4i extended = extendLine(minY, maxY, line);
    
    return {{float(extended[0]),float(extended[1])},{float(extended[2]),float(extended[3])}};
}


void sortCorners(std::vector<cv::Point2f>& corners, cv::Point2f center)
{
	std::vector<cv::Point2f> top, bot;

	for (size_t i = 0; i < corners.size(); i++)
	{
		if (corners[i].y < center.y)
			top.push_back(corners[i]);
		else
			bot.push_back(corners[i]);
	}

	cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
	cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
	cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
	cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];

	corners.clear();
	corners.push_back(tl);
	corners.push_back(tr);
	corners.push_back(br);
	corners.push_back(bl);
}



vector<cv::Point2f> transformLine(cv::Mat transform, vector<cv::Point2f>line) {
    
   
    
    vector<cv::Point2f> dest_lines(line.size());
    
    perspectiveTransform(line, dest_lines, transform);
    
    return dest_lines;
}

vector<cv::Point2f> transformLine(std::vector<cv::Point2f> original, std::vector<cv::Point2f> dest, vector<cv::Point2f>line) {

	auto transformMatrix = getPerspectiveTransform(original, dest);

	vector<cv::Point2f> dest_lines(line.size());

	perspectiveTransform(line, dest_lines, transformMatrix);

	return dest_lines;
}


vector<Vec4i> filterLines(vector<Vec4i> original) {

	vector<Vec4i> output;


	for_each(original.begin(), original.end(), [&](Vec4i elem) {

		bool add = true;

		float m = float(elem[3] - elem[1]) / float(elem[2] - elem[0]);

		for_each(output.begin(), output.end(), [&](Vec4i selectedElem) {


			float selected_m = float(selectedElem[3] - selectedElem[1]) / float(selectedElem[2] - selectedElem[0]);

			if (abs(selected_m - m) < 5 * CV_PI / 180) {
				add = false;
			}


		});



		if (add)output.push_back(elem);


	});

	return output;

}

vector<Vec4i> Image::lines(Image canny) {


	vector<Vec4i> lines;
    
    HoughLinesP(canny.matrix, lines, 1, CV_PI / 180, 100, this->matrix.cols / 4.0, this->matrix.cols / 3);

    
	for (size_t i = 0; i < lines.size(); i++)
	{
		cv::Vec4i v = lines[i];
		lines[i][0] = 0;
		lines[i][1] =  (int)(((float)v[1] - v[3]) / (v[0] - v[2]) * -v[0] + v[1]);
		lines[i][2] = this->matrix.cols;
		lines[i][3] = (int) (((float)v[1] - v[3]) / (v[0] - v[2]) * (this->matrix.cols - v[2]) + v[3]);



	}

    
	return filterLines(lines);



}


cv::Point2f computeIntersect(cv::Vec4i a, cv::Vec4i b)
{
	int x1 = a[0], y1 = a[1], x2 = a[2], y2 = a[3];
	int x3 = b[0], y3 = b[1], x4 = b[2], y4 = b[3];

	if (float d = ((float)(x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)))
	{
		cv::Point2f pt;
		pt.x = ((x1*y2 - y1*x2) * (x3 - x4) - (x1 - x2) * (x3*y4 - y3*x4)) / d;
		pt.y = ((x1*y2 - y1*x2) * (y3 - y4) - (y1 - y2) * (x3*y4 - y3*x4)) / d;
		return pt;
	}
	else
		return cv::Point2f(-1, -1);
}


bool isDummyPerspectiveConf(PerspectiveConf conf) {

	return conf.original.size() == 0;

}


Image Image::linesImage_points(Image cannyRawOutputImage, vector<cv::Point2f> lines) {


	vector<cv::Vec4i> result;

	for (size_t i = 0; i < lines.size(); i = i + 2) {

		Vec4i vec;
		vec[0] = (int) lines[i].x;
		vec[1] = (int) lines[i].y;
		vec[2] = (int)lines[i + 1].x;
		vec[3] = (int) lines[i + 1].y;

		result.push_back(vec);


	}

	return linesImage(cannyRawOutputImage, result);


}

Image Image::linesImage(Image cannyRawOutputImage, vector<cv::Vec4i> lines) {

	if (lines.size() == 0) {

		lines = this->lines(cannyRawOutputImage);
	}


	cv::Scalar color;
	if (this->type == GrayScale) {
		color = 200;
	}
	else {

		color = CV_RGB(255, 0, 0);;

	}

	auto out_matrix = this->matrix.clone();



	vector<float> f;

	for (size_t i = 0; i < lines.size(); i++)
	{

		cv::Vec4i v = lines[i];
		lines[i][0] = 0;
		lines[i][1] = (int) (((float)v[1] - v[3]) / (v[0] - v[2]) * -v[0] + v[1]);
		lines[i][2] = cannyRawOutputImage.matrix.cols;
		lines[i][3] = (int) (((float)v[1] - v[3]) / (v[0] - v[2]) * (cannyRawOutputImage.matrix.cols - v[2]) + v[3]);


		line(out_matrix, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), color, 1, 8);

	}




	return Image(out_matrix, this->type);

}


Image Image::linesImageContinuos(Image grayscaleLined) {
	vector<Vec2f> lines;

	HoughLines(grayscaleLined.matrix, lines, 1, CV_PI / 180, 70);

	cv::Mat outMat = this->matrix.clone();

	cv::Scalar color;
	if (this->type == GrayScale)
		color = 255;
	else
		color = CV_RGB(255, 0, 0);

	std::vector<cv::Vec2f>::const_iterator it = lines.begin();
	while (it != lines.end()) {
		float rho = (*it)[0]; // first element is distance rho
		float theta = (*it)[1]; // second element is angle theta
		if (theta < CV_PI / 4.
			|| theta > 3. * CV_PI / 4.) { // ~vertical line
									 // point of intersection of the line with first row
			cv::Point pt1((int) (rho / cos(theta)), 0);
			// point of intersection of the line with last row
			cv::Point pt2((int) ((rho - outMat.rows * sin(theta)) /
				cos(theta)), outMat.rows);
			// draw a white line
			cv::line(outMat, pt1, pt2, color, 1);

		}
		else { // ~horizontal line
			   // point of intersection of the
			   // line with first column
			cv::Point pt1(0, (int) (rho / sin(theta)));
			// point of intersection of the line with last column
			cv::Point pt2(outMat.cols, (int)
				((rho - outMat.cols*cos(theta)) / sin(theta)));
			// draw a white line
			cv::line(outMat, pt1, pt2, color, 1);
		}
		++it;
	}

	return Image(outMat, GrayScale);
}


Image Image::distortImage(PerspectiveConf conf) {





	cv::Mat quad = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC3);


	// Get transformation matrix
	cv::Mat transmtx = cv::getPerspectiveTransform(conf.original, conf.dest);

	// Apply perspective transformation
	cv::warpPerspective(this->matrix, quad, transmtx, quad.size());



	return Image(quad, this->type);


}


PerspectiveConf Image::distortParameters(int lowerThreshold, int upperThreshold, bool isCanny) {
	Image canny;
	vector<Vec4i> lines;
    

	if (!isCanny) {
		//TODO CHECK IF IS BEST TO USE GAUSSIAN BLUR INSTEAD OF CUSTOM EDGE
		canny = customCannyProcess(lowerThreshold, upperThreshold);
		lines = canny.lines(canny);
	}
	else
		lines = this->lines(*this);


    
    

	vector<Vec4i> linesForEdges;



	Vec4i leftLine, rightLine;

	
    /*
    bool foundLeft = false, foundRight = false;


	for (int i = 0; i < lines.size(); i++) {

		Vec4i elem = lines[i];
		float m = slope(elem);
		if (abs(m) > 0.2 && abs(m) < 0.9) {

			

			if (m < 0) {

				leftLine = elem;
				foundLeft = true;

			}

			if (m > 0) {

				rightLine = elem;
				foundRight = true;
			}

		}


	}

	if (!foundRight && !foundLeft) return PerspectiveConf();
    
    

	linesForEdges.push_back(leftLine);
	linesForEdges.push_back(rightLine);



	cv::Point2f linesIntersection = computeIntersect(linesForEdges[0], linesForEdges[1]);


     */
    
    
    Vec4i chosenLine = {0,0,0,0};
    Vec4i mirrorLine = {0,0,0,0};
    
   
    float best = (float) this->matrix.rows;
    
    
    for_each(lines.begin(), lines.end(), [&best,&mirrorLine,this,&chosenLine](Vec4i line){
        
        if (abs(slope(line))<0.2 || abs(slope(line))>0.9 ) {
            return;
        }
        
        mirrorLine[0] = this->matrix.cols/2 + this->matrix.cols/2-line[0];
        
        mirrorLine[2] = this->matrix.cols/2 + this->matrix.cols/2-line[2];
        
        mirrorLine[1] = line[1];
        
        mirrorLine[3] = line[3];
        
        cv::Point2f linesIntersection = computeIntersect(chosenLine, line);
        
        if(linesIntersection.y < best){
            
            chosenLine = line;
            best = linesIntersection.y;
            
        }
    
    
    });
    
    
    
    mirrorLine[0] = this->matrix.cols;
    
    mirrorLine[2] = 0;
    
    mirrorLine[1] = chosenLine[1];
    
    mirrorLine[3] = chosenLine[3];
    
    cv::Point2f linesIntersection = computeIntersect(chosenLine, mirrorLine);
    
    
    
    
    
    
    linesForEdges.push_back(chosenLine);
    linesForEdges.push_back(mirrorLine);
    
    
    
    
    
    

	Vec4i horizontalLine1;

	horizontalLine1[0] = 0;
	horizontalLine1[1] = this->matrix.rows;
	horizontalLine1[2] = this->matrix.cols;
	horizontalLine1[3] = this->matrix.rows;


	Vec4i horizontalLine2;


	horizontalLine2[0] = 0;
	horizontalLine2[1] = (int) (linesIntersection.y*1.3);
	horizontalLine2[2] = this->matrix.cols;
	horizontalLine2[3] = (int) (linesIntersection.y*1.3);


	std::vector<cv::Point2f> corners;

    

	corners.push_back(computeIntersect(horizontalLine1, linesForEdges[0]));
	corners.push_back(computeIntersect(horizontalLine1, linesForEdges[1]));

	corners.push_back(computeIntersect(horizontalLine2, linesForEdges[0]));
	corners.push_back(computeIntersect(horizontalLine2, linesForEdges[1]));


    
    /*
    
    corners.push_back(Point2f(-1*chosenLine[1]/slope(chosenLine),0));
    corners.push_back(Point2f(this->matrix.cols - corners[0].x,0));
    
    corners.push_back(Point2f(this->matrix.cols/2 + 0.3 * linesIntersection.y/slope(chosenLine) ,linesIntersection.y*1.3));
    corners.push_back(Point2f(this->matrix.cols/2 - 0.3 * linesIntersection.y/slope(chosenLine),linesIntersection.y*1.3));
*/

	cv::Point2f center(0, 0);
	for (size_t i = 0; i < corners.size(); i++)
		center += corners[i];

	center *= (1. / corners.size());
	sortCorners(corners, center);


	cv::Mat quad = cv::Mat::zeros(this->matrix.rows, this->matrix.cols, CV_8UC3);

	// Corners of the destination grayscale
	std::vector<cv::Point2f> quad_pts;
	quad_pts.push_back(cv::Point2f( (float) (quad.cols / 4.0), (float) 0.0));
	quad_pts.push_back(cv::Point2f((float) (quad.cols * 3 / 4.0), (float) 0.0));
	quad_pts.push_back(cv::Point2f((float)(quad.cols * 3 / 4.0), (float) quad.rows));
	quad_pts.push_back(cv::Point2f((float)(quad.cols / 4.0), (float) quad.rows));



	PerspectiveConf conf;
	conf.original = corners;
	conf.dest = quad_pts;
	return conf;

}

Image Image::customCannyProcess(int &lowerTreshold, int &upperTreshold, cv::Size size)
{

	return getBlurredCustomEdgeImage(size).cannyImageAuto(lowerTreshold, upperTreshold);
}

Image Image::getBlurredCustomEdgeImage(cv::Size size)
{

	int tau = 15;
	Image gaussianBlurCanny = this->grayScaleImage();

	//using gaussian to give more importance to the closest pixels while determining the blur
	cv::Mat blurResult;
	double sigma = 6;	//sigma relates the influence of pixels with the distance to them, normal distribution
	cv::Size kernelSize = size;

	cv::GaussianBlur(gaussianBlurCanny.matrix, blurResult, kernelSize, sigma);
	gaussianBlurCanny.matrix = blurResult;

	gaussianBlurCanny = gaussianBlurCanny.customEdgeDetectorImage(15, true);


	cv::GaussianBlur(gaussianBlurCanny.matrix, blurResult, kernelSize, sigma);
	gaussianBlurCanny.matrix = blurResult;
	return Image(gaussianBlurCanny.matrix, GrayScale);
}

cv::Mat setRoiForLines(cv::Mat &grayscale, cv::Vec4i const &leftLine, cv::Vec4i const &rightLine, int tau) {

	cv::Mat output = cv::Mat::zeros(grayscale.rows, grayscale.cols, CV_8UC1);
	cv::Vec4i extendedLine1 = extendLine(0, grayscale.rows, leftLine);
	cv::Vec4i extendedLine2 = extendLine(0, grayscale.rows, rightLine);


	cv::Point2f intersect = computeIntersect(extendedLine1, extendedLine2);

	int intersectY = (int)intersect.y;

	float slope1 = inv_slope(extendedLine1);
	float slope2 = inv_slope(extendedLine2);


	int leftPos = extendedLine1[2];
	int rightPos = extendedLine2[2];

	for (int j = output.rows-1; j >=intersectY && j > 0; j--) {

		uchar *ptrRowdst = output.ptr<uchar>(j);
		uchar *ptrRowSrc = grayscale.ptr<uchar>(j);

		int leftPixel = leftPos;
		int rightPixel = rightPos;
        
        
        
        
        
        leftPixel = extendedLine1[2] + slope1 * (j - grayscale.rows);
        rightPixel = extendedLine2[2] + slope2 * (j -grayscale.rows);
        
        for (int i = max(0,leftPixel-tau) ; i < min(rightPixel+tau,grayscale.cols); i++) {
            
            ptrRowdst[i] = ptrRowSrc[i];
            
        }
        
        /*

        
		for (int window = -tau; window < tau; window++) {

			leftPixel =(int) (extendedLine1[2] + slope1*(-grayscale.rows + j) + window);
			leftPixel = max(0, leftPixel);
			rightPixel = (int) (extendedLine2[2] + slope2*(-grayscale.rows + j) + window);
			rightPixel = min(rightPixel, grayscale.cols);

			ptrRowdst[rightPixel] = ptrRowSrc[rightPixel];
			ptrRowdst[leftPixel] = ptrRowSrc[leftPixel];;

		}
         */
         
	}


	return output;
}


