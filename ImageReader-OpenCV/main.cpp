#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "Image.hpp"
#include "Video.hpp"
#include <thread>
#include <iostream>
#include <algorithm>
#include <functional>
#include <iostream>


#include <vector>
#include <numeric>
#include <string>

using namespace std;
using namespace cv;


//=============== Videos ===============
string videoHighwayMp4 = "resources/highway.mp4";



void roadProcessingVideo(string path, int secondsOffset);
void alternativeRoadProcessing();
cv::Mat lanesLinesAndArea(cv::Mat &frame, Image &distorted, PerspectiveConf &conf);
cv::Vec4i lineInRoi(cv::Vec4i lastDetected, cv::Mat transformedImage, int tauRoi);


inline float inv_slope(Vec4i line) {

	return float(line[2] - line[0]) / float(line[3] - line[1]);

}

vector<Vec4i> __filterLines(vector<Vec4i> lines,Mat frame,vector<Vec4i> last){
    
    
    auto lastLeft = last[0];
    auto lastRight = last[1];
    
    
    lines.erase(remove_if(lines.begin(), lines.end(), [&frame](Vec4i line){
        
        
        auto extended = extendLine(0, frame.rows, line);
        
        if(abs(inv_slope(line)) < 0.1) return true; // remove vertical lines
        if(abs(slope(line)) < 0.1 ) return false; // remove horizontal lines
        
        if(abs(extended[0] - frame.cols/2) < frame.cols/8 )return true; // remove lines at the center
        
        return false;
        
    }),lines.end());
    
    
    
    
    
    
    
    
    
    auto leftLines = accumulate(lines.begin(), lines.end(),vector<Vec4i>(),[&lastLeft,&frame](vector<Vec4i> acc,Vec4i line){
        
        if (slope(line) < 0){ // discard right lines
            
            
            auto extended = extendLine(0, frame.rows, line);
            
            if(extended[2] < frame.cols/2 && abs(extended[2]-frame.cols/2) > frame.cols/10){ // discard lines too on the right or too on the center
                acc.push_back(line);
            }
            
        }
        
        return acc;
        
    });
    
    
    auto bestLeft = accumulate(leftLines.begin(), leftLines.end(), Vec4i{0,0,0,0},[&lastLeft,&frame](Vec4i best,Vec4i line){
        
        if(best == Vec4i{0,0,0,0})return line;
        
        
        auto ext_line = extendLine(0, frame.rows, line);
        auto ext_best = extendLine(0, frame.rows, best);
        
        
        if (ext_line[2] > ext_best[2] && ext_line[0] >= ext_best[0])return line; // choose the one closest to the center
        
        return best;
        
        
        
    });
    
    
    
    
    
    auto rightLines = accumulate(lines.begin(), lines.end(),vector<Vec4i>(),[&lastLeft,&frame](vector<Vec4i> acc,Vec4i line){
        
        if (slope(line) > 0){ // discard right lines
            
            
            auto extended = extendLine(0, frame.rows, line);
            
            if(extended[2] > frame.cols/2 && abs(extended[2]-frame.cols/2) > frame.cols/10){ // discard lines too on the left or too on the center
                acc.push_back(line);
            }
            
            
            
            
        }
        
        return acc;
        
    });
    
    
    


    
    auto bestRight = accumulate(rightLines.begin(), rightLines.end(), Vec4i{0,0,0,0},[&lastRight,&frame](Vec4i best,Vec4i line){
        
        if(best == Vec4i{0,0,0,0})return line;
        
        
        
        auto ext_line = extendLine(0, frame.rows, line);
        auto ext_best = extendLine(0, frame.rows, best);
        
        
        if (ext_line[2] < ext_best[2] && ext_line[0] < ext_best[0])return line;// choose the one closest to the center
        
        
        
        
        return best;
        
        
        
    });
    
    return {bestLeft,bestRight};
    
}

int lineDist(Vec4i line1,Vec4i line2,int maxY){
    
    
    auto line1_ext = extendLine(0, maxY, line1);
    auto line2_ext = extendLine(0, maxY, line2);
    
    return abs(line1_ext[0]-line2_ext[0]) + abs(line1_ext[2]-line2_ext[2]);
    
    
}

int main(int argc, char** argv)
{
	cout << "Usage: " << endl << "program path_to_video starting_video_seconds" << endl
		<< "If no path to video file is given, program will assume a default video (./resources/highway.mp4) starting at second 0" << endl
		<< "If no starting second is given, program will assume the video starts at second 0" << endl;


	std::string filePath = videoHighwayMp4;
	int startingSecond = 0;

	
	if(argc == 2)
		filePath = string(argv[1]);
	else if (argc == 3) {
		filePath = string(argv[1]);
		stringstream ss(argv[2]);
		if (!(ss >> startingSecond)) {
			startingSecond = 0;
			cout << "Invalid starting video second given" << endl;
		}
	}

	roadProcessingVideo(filePath, startingSecond);
	waitKey();
	return 0;
}

/**
Draws a filled circled in the matrix image at given point
*/
void drawColouredVanishingPoint(cv::Mat &image, const Point2f &point) {
	cv::circle(image, point, 5, CV_RGB(0, 255, 0), -1);
}

/**
Draws the two given lines in image and then executes the floodfill algorithm between the given point and the image bottom
*/
void linesAndFloodFill(cv::Mat &image, const Point2f &point, const cv::Vec4i &line1, const cv::Vec4i &line2) {

	//cv::Mat blend = cv::Mat::zeros(image.size(), image.type());
	cv::line(image, { line1[0],line1[1] }, { line1[2],line1[3] }, CV_RGB(255, 255, 255));
	cv::line(image, { line2[0],line2[1] }, { line2[2],line2[3] }, CV_RGB(255, 255, 255));

	Point2f seedPoint = { point.x, point.y + (image.rows - point.y) / 2 };
	cv::floodFill(image, seedPoint, CV_RGB(20, 20, 200), nullptr, CV_RGB(20, 20, 20), CV_RGB(20, 20, 20), 4);
}

/**
Draws the lines in the image, runs floodfill algorithm between them and draws the vanishing point
*/
void floodFillVanishingPt(cv::Mat &image, const Point2f &point, const cv::Vec4i &line1, const cv::Vec4i &line2) {
	cv::Mat blend = cv::Mat::zeros(image.size(), image.type());
	
	linesAndFloodFill(blend, point, line1, line2);
	drawColouredVanishingPoint(blend, point);
	cv::addWeighted(image, 0.7, blend, 0.3, 0.0, image);
}


void roadProcessingVideo(string path, int secondsOffset) {

	Video roadTestVideo(path);
	roadTestVideo.fastforwardSec(secondsOffset);

	cv::Mat transformMatrix;
	cv::Mat inv_transformMatrix;
	bool transformMatrixPresent = false;
	bool safeToGo = false;

	int nrFramesMissed = 0;

	Vec4i lastLeft = {0,0,0,0};
	Vec4i lastRight = {0,0,0,0};

    int missedLeft=0,missedRight=0;


	roadTestVideo.addDisplayer("Video", [&missedLeft,&missedRight,&safeToGo, &inv_transformMatrix, &lastLeft, &lastRight, &transformMatrix, &transformMatrixPresent, &nrFramesMissed](cv::Mat frame) {

		std::vector<cv::Mat> output_frames;

        Mat custom = customEdgeDetectorImage(frame, 10); // custom edge detection algorithm
        
        Mat forClear = custom.clone(); // clone matrix

        GaussianBlur(custom, custom, {9,9}, 6); // apply gaussian blur

        threshold(custom, custom, 25, 255, THRESH_BINARY);  // apply thresholding

        thinning(custom); // apply thinning algorithm

        GaussianBlur(forClear, forClear, {3,3}, 6);
        
        threshold(forClear, forClear, 25, 255, THRESH_BINARY);
        
        thinning(forClear);
        
        Vec4i bestLeft = {0,0,0,0},bestRight = {0,0,0,0};
        
        auto clear = setRoiForLines(forClear, lastLeft, lastRight, 5);
        
        output_frames.push_back(clear);

        vector<Vec4i> lines;

        HoughLinesP(clear, lines, 1,   2 * CV_PI / 180, 10, frame.rows / 30.0, frame.rows);
        
        auto best = __filterLines(lines,frame,{lastLeft,lastRight});
        
        lines = vector<Vec4i>();
        
        HoughLinesP(custom, lines, 1,   5 * CV_PI / 180, 10, frame.rows / 15.0, frame.rows / 3);

        auto best2 = __filterLines(lines, frame, {lastLeft,lastRight});
        
		const cv::Vec4i zeroVector = Vec4i(0, 0, 0, 0);
        
        if(best[0] != zeroVector){
            bestLeft = best[0];
            missedLeft = 0;
        }
        else{
            missedLeft++;
        }
        
        if(bestLeft == zeroVector && missedLeft > 3){
            bestLeft = best2[0];
        }
        
        if(best[1] != zeroVector){
            bestRight = best[1];
            missedRight = 0;
        }
        else{
            missedRight++;
        }
        
        if(bestRight == zeroVector && missedRight > 3){
            bestRight = best2[1];
        }
        
        //if not from best1 or best2

        if (bestLeft != zeroVector) {

            lastLeft = bestLeft;

        }
        
        if (bestRight != zeroVector) {
            
            lastRight = bestRight;
            
        }
        
		bool hasLeft = false,
			hasRight = false;
		cv::Vec4i extendedLeft,
			extendedRight;
        
        if (lastLeft != zeroVector){

            extendedLeft = extendLine(0, frame.cols, lastLeft);
            cv::line(frame, {extendedLeft[0],extendedLeft[1]}, {extendedLeft[2],extendedLeft[3]}, CV_RGB(255, 255, 255));
			hasLeft = true;
        }

        if (lastRight != zeroVector){

            extendedRight = extendLine(0, frame.cols, lastRight);
            cv::line(frame, {extendedRight[0],extendedRight[1]}, {extendedRight[2],extendedRight[3]}, CV_RGB(255, 255, 255));
			hasRight = true;
        }

		if (hasRight && hasLeft) {
			Point2f intersection = computeIntersect(extendedLeft, extendedRight);
			if (intersection.x > 0 && intersection.x < frame.cols
				&& intersection.y > 0 && intersection.y < frame.rows) {

				floodFillVanishingPt(frame, intersection, extendedLeft, extendedRight);


			}
		}

		output_frames.push_back(frame);
        output_frames.push_back(custom);
        
		return output_frames;

	});

	roadTestVideo.display();

}


cv::Vec4i lineInRoi(cv::Vec4i lastDetected, cv::Mat transformedImage, int tauRoi) {
	if (lastDetected != cv::Vec4i{ 0,0,0,0 }) {

		cv::Vec4i tmp = extendLine(0, transformedImage.rows, lastDetected);

		int pixel = max(min(tmp[0],tmp[2]), tauRoi);
        int width = min(transformedImage.cols-pixel,2*tauRoi);

	
		cv::Rect roiRect = Rect(pixel-tauRoi, 0, width, tmp[3]);
		cv::Mat roi;
		try {
			roi = transformedImage(roiRect);
			roi = transformedImage(roiRect).clone();
			GaussianBlur(roi, roi, { 1,5 }, 6);
			cv::threshold(roi, roi, 80, 255, cv::THRESH_BINARY);
			thinning(roi);

			vector<cv::Vec4i> lines;

			HoughLinesP(roi, lines, 10, 3 * CV_PI / 180, 100, transformedImage.rows / 20, transformedImage.rows);
            
            
            
            if (lines.size() == 0) return Vec4i{0,0,0,0};
            
            Vec4i toReturn = accumulate(lines.begin(), lines.end(), lines[0],[&lastDetected](Vec4i best,Vec4i current){
            
                
                if(abs(inv_slope(current)-inv_slope(lastDetected)) < abs(inv_slope(best)-inv_slope(lastDetected))){
                    return current;
                }
                /*
                if(abs(current[0]-lastDetected[0]) + abs(current[2]-lastDetected[2]) < abs(best[0]-lastDetected[0]) + abs(best[2]-lastDetected[2])){
                    return current;
                    
                }
                */
                return best;
            
                
            });
            
            toReturn[0] += pixel;
            toReturn[2] += pixel;
		}
		catch (Exception e) {
			cout << e.msg << endl;
		}


	}
	return cv::Vec4i{ 0,0,0,0 };
}

cv::Mat lanesLinesAndArea(cv::Mat &frame, Image &distorted, PerspectiveConf &conf) {

	int window = 50;

	cv::Mat output = cv::Mat::zeros(distorted.matrix.size(), distorted.matrix.type());

	vector<int> lines;

	int mostLeft = 0;
	int mostRight = output.cols;

	int left = 0;
	int right = output.cols;

	uchar threashold = 20;

	int halfCols = distorted.matrix.cols / 2;

	//QUESTION porque de dentro para fora? Porque ignorar 2/5 da imagem central?
	for (int middleShift = distorted.matrix.cols / 5; middleShift < halfCols; middleShift++)
	{

		int counterLeft = 0;
		int counterRight = 0;

		for (int line = window; line < distorted.matrix.rows - window; line++)
		{

			uchar pointLeft = distorted.matrix.at<uchar>(Point(halfCols - middleShift, line));
			uchar pointRight = distorted.matrix.at<uchar>(Point(halfCols + middleShift, line));

			if (pointLeft > threashold) counterLeft++;
			if (pointRight > threashold) counterRight++;
		}


		int currentRightLine = distorted.matrix.cols / 2 + middleShift;
		int currentLeftLine = distorted.matrix.cols / 2 - middleShift;

		if (counterRight > output.rows / 30) //LINES HEIGHT IS TALLER THAN 30% OF THE IMAGE
		{

			if (right == output.cols)
			{

				right = currentRightLine;
			}
			else if (mostRight == output.cols && abs(currentRightLine - right) > distorted.matrix.cols / 20) {

				mostRight = currentRightLine;
			}

		}

		if (counterLeft > output.rows / 30)
		{

			if (left == 0) {

				left = currentLeftLine;
			}
			else if (mostLeft == 0 && abs(currentLeftLine - left) > distorted.matrix.cols / 20)
			{
				mostLeft = currentLeftLine;
			}

		}

	}

	Point2f left_line, right_line, mostLeft_line, mostRight_line;

	vector<cv::Point2f> points_vector;

	points_vector.push_back(Point(mostLeft, 0));
	points_vector.push_back(Point(mostLeft, output.rows));
	points_vector.push_back(Point(mostRight, 0));
	points_vector.push_back(Point(mostRight, output.rows));

	cv::line(output, Point(mostLeft, 0), Point(mostLeft, output.rows), 255, 3);
	cv::line(output, Point(mostRight, 0), Point(mostRight, output.rows), 255, 3);

	cv::Mat back = cv::Mat::zeros(output.size(), output.type());

	cv::Mat transmtx = cv::getPerspectiveTransform(conf.dest, conf.original);

	points_vector = transformLine(conf.dest, conf.original, points_vector);

	cv::warpPerspective(output, back, transmtx, output.size());

	//line(back, points_vector[0],points_vector[1], 255,1);

	Image backImage(back, GrayScale);

	Image withLines = backImage.linesImage_points(backImage, points_vector);

	cv::floodFill(withLines.matrix, Point(withLines.matrix.cols / 2, withLines.matrix.rows - 1), 100, nullptr, 50, 50, 4);

	points_vector = vector<Point2f>();
	points_vector.push_back(Point(left, 0));
	points_vector.push_back(Point(left, output.rows));
	points_vector.push_back(Point(right, 0));
	points_vector.push_back(Point(right, output.rows));

	points_vector = transformLine(conf.dest, conf.original, points_vector);

	withLines = withLines.linesImage_points(backImage, points_vector);

	cv::floodFill(withLines.matrix, Point(withLines.matrix.cols / 2, withLines.matrix.rows - 1), 200, nullptr, 50, 50, 4);

	cv::Mat mat;

	withLines = withLines.rgbImage();

	//return withLines.matrix;

	cv::addWeighted(withLines.matrix, 0.5, frame, 0.5, 0.0, mat);

	return mat;
}

