#include "Video.hpp"
#include <iostream>


Video::Video(int camera) {
	this->captureFrameKeyCode = -1;
	this->processor = nullptr;

	this->cap = cv::VideoCapture(camera);

}




Video::Video(std::string file) {

	this->captureFrameKeyCode = -1;
	this->processor = nullptr;

	this->cap = cv::VideoCapture(file);
	if (!cap.isOpened())  // check if we succeeded
		std::cout << "Could not open" << std::endl;
}





void Video::addDisplayer(string name, function<std::vector<cv::Mat>(cv::Mat)> processor) {

	displayers.push_back(Displayer(name, processor));

}

void Video::addKeyAction(int keyCode, std::function<void(Video*, cv::Mat frame)> action) {


	keyActions.push_back(KeyAction(keyCode, action));


}

void Video::saveFrame(cv::Mat frame) {

	this->capturedFrame = frame;

}

void Video::display() {

	int action = 0;
	int keyPressWaitingTime = 30;
	cv::Mat frame;
	bool bSuccess = true;
	int key = 0;

	VIDEO_MODE mode = PLAY;

	while (1) {

		key = cv::waitKey(keyPressWaitingTime);


		switch (key)
		{
		case ASCII_ARROW_UP:
			mode = PLAY;
			break;
		case ASCII_ARROW_RIGHT:
			mode = FRAME;
			if (this->cap.grab())
				bSuccess = this->cap.retrieve(frame);
			else {
				std::cout << "Cannot read a frame from video stream" << std::endl;
				return;
			}
		default:
			break;
		}


		switch (mode)
		{
		case Video::PLAY:
			keyPressWaitingTime = 30; //wait 30 ms to a key be pressed
			bSuccess = cap.read(frame);
			break;
		case Video::FRAME:
			keyPressWaitingTime = 0; //wait until some key is pressed
			break;
		default:
			break;
		}

		if (!bSuccess) //if not success, break loop
		{
			std::cout << "Cannot read a frame from video stream" << std::endl;
			//continue;
			return;
		}

		std::for_each(displayers.begin(), displayers.end(), [&frame](Displayer d) {

			d.display(frame);

		});


		std::for_each(keyActions.begin(), keyActions.end(), [this, &frame, key](KeyAction a) {

			if (key == a.keyValue) {

				a.action(this, frame);

			}

		});

	}


}

void Video::fastforwardSec(int s) {
	this->cap.set(CV_CAP_PROP_POS_MSEC, s * 1000);
}

void Video::fastforwardMSec(int ms) {
	this->cap.set(CV_CAP_PROP_POS_MSEC, ms);
}



